import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FlightFormPage {

    WebDriver driver;
    @FindBy(id = "flight_form_FirstName")
    WebElement firstName;
    @FindBy(id = "flight_form_LastName")
    WebElement lastName;
    @FindBy(id = "flight_form_email")
    WebElement email;
    @FindBy(id = "flight_form_BirthDate")
    WebElement birthday;
    @FindBy(id = "flight_form_submit")
    WebElement sendButton;

    public FlightFormPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.get("https://flight-order.herokuapp.com");
    }

    public void setFirstName(String firstNameStr) {
        this.firstName.sendKeys(firstNameStr);
    }

    public void setLastName(String lastNameStr) {
        this.lastName.sendKeys(lastNameStr);
    }

    public void setEmail(String emailStr) {
        this.email.sendKeys(emailStr);
    }

    public void setBirthday(String birthdayStr) {
        this.birthday.sendKeys(birthdayStr);
    }

    public void setSendButton() {
        this.sendButton.click();
    }
}
