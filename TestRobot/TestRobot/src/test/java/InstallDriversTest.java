
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

class InstallDriversTest {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().fullscreen();
        driver.get("https://moodle.fel.cvut.cz");


    }

    @Test
    public void chromeSession() {
        WebElement loginLink = driver.findElement(By.cssSelector("#navbarSupportedContent > ul > div.d-flex.flex-row.flex-wrap.align-items-center.no-gutters.pt-3.pt-md-0 > li.d-flex.align-items-center.ml-auto > a"));
        loginLink.click();
        WebElement form = driver.findElement(By.id("sso-form"));
        form.click();
        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("lushnalv");
        WebElement password = driver.findElement(By.id("password"));
        password.sendKeys("Hvezda4321");
        WebElement button = driver.findElement(By.name("_eventId_proceed"));
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(button));
        button.click();

        driver.get("https://moodle.fel.cvut.cz/course/view.php?id=6692");

        WebElement test = driver.findElement(By.cssSelector("#module-233484 > div > div > div:nth-child(2) > div > a"));
        driver.get(test.getAttribute("href"));

        List<WebElement> buttonReattempt = driver.findElements(By.tagName("form"));
        buttonReattempt.get(0).submit();
        List<WebElement> buttonReattemptTrue = driver.findElements(By.tagName("form"));
        buttonReattemptTrue.get(0).submit();

        WebElement ourForm = driver.findElements(By.tagName("form")).get(0);
        WebElement textarea = ourForm.findElement(By.tagName("textarea"));
        WebElement input = ourForm.findElements(By.tagName("input")).stream().filter(it -> it.getAttribute("id").contains("2_answer")).findFirst().get();
        List<WebElement> selects = ourForm.findElements(By.tagName("select"));
        textarea.sendKeys("Alvina CV 11");
        input.sendKeys("86400");
        new Select(selects.get(0)).selectByVisibleText("Oberon");
        new Select(selects.get(1)).selectByVisibleText("Rumunsko");


    }

    @Test
    public void chromelogin() {
        String strUrl = driver.getCurrentUrl();
        driver.get(driver.getCurrentUrl());


    }

}