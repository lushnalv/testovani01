package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class MockitoMailHelperTest {


    @Test
    public void sendMail_validEmail_saveMailToDB() {
        DBManager mockDbManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockDbManager);
        int mailID = 1;
        Mail mailToReturn = mock(Mail.class);

        when(mailToReturn.getTo()).thenReturn("");
        when(mailToReturn.getSubject()).thenReturn("");
        when(mailToReturn.getBody()).thenReturn("");
        when(mockDbManager.findMail(anyInt())).thenReturn(mailToReturn);

        mailHelper.sendMail(mailID);

        verify(mockDbManager, times(1)).findMail(mailID);
        verify(mockDbManager, times(1)).saveMail(any(Mail.class));

    }

}
