package lab3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FooTest {
    @Test
    public void Return_Number_ReturnSpecifiedNumber_five(){
        Foo foo = new Foo();
        Assertions.assertEquals(5, foo.returnNumber());
    }
}
