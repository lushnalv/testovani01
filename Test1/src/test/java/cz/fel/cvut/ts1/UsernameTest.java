package cz.fel.cvut.ts1;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.*;
public class UsernameTest {
    @Test
    public void factorialTest(){
        Username username = new Username();

        assertEquals(username.factorial(-1), -1L);
        assertEquals(username.factorial(2), 2L);
        assertEquals(username.factorial(3), 6L);
        assertEquals(username.factorial(4), 24L);
        assertEquals(username.factorial(5), 120L);
        assertEquals(username.factorial(6), 720L);
        assertEquals(username.factorial(7), 5040L);
    }

}
