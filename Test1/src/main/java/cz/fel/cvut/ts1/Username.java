package cz.fel.cvut.ts1;
public class Username {
    public long factorial(int n) {
        if (n == 0) {
            return (long) 1;
        } else if (n > 0) {
            return (long) n * factorial(n - 1);
        }
        return -1;
    }

}

