import hw2.Calculator;
import hw2.exeptions.DivideByZeroException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTests {
    Calculator calculator;
    @BeforeEach
    public void singleTestSetUP(){
        calculator = new Calculator();
    }
    @Test
    public void add_test(){
        Assertions.assertEquals(2, calculator.add(1,1));

    }
    @Test
    public void subtract_test(){
        Assertions.assertEquals(2, calculator.subtract(3,1));

    }
    @Test
    public void multiply_test(){
        Assertions.assertEquals(4, calculator.multiply(2,2));

    }

    @Test
    public void divide_test() throws DivideByZeroException {
        Assertions.assertEquals(5, calculator.divide(10,2));
    }

    @Test
    public void divide_by_zero_test() {
        Assertions.assertThrows(DivideByZeroException.class, ()->calculator.divide(2,0));

    }

    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5"})
    @CsvFileSource(resources = "/data.csv")
    public void add_addsAndB_returnsC(int a, int b, int c) {
// arrange
        Calculator calc = new Calculator();
// assert
        assertEquals(c, calc.add(a, b));
    }





}
