package hw2;

import hw2.exeptions.DivideByZeroException;

public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int divide(int a, int b) throws DivideByZeroException {
        try {
            return a / b;
        } catch (ArithmeticException e) {

            throw new DivideByZeroException("Can't divide by zero!!!");
        }
    }


}
