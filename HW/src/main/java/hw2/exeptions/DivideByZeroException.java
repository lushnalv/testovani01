package hw2.exeptions;

public class DivideByZeroException extends Exception{

    public DivideByZeroException(String message) {
        super(message);
    }

}
