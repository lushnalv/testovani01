package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {
    String customerName = "Alvina";
    String customerAddress = "Praha, Parikova, 94/6";
    int state = 1;

    @Test
    public void Constructor_ShoppingCartNull_NullPointerException(){
        Assertions.assertThrows(NullPointerException.class, () -> new Order(null, customerName, customerAddress, state));
    }

    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
    public void Constructor_CreateOrderWithState_OrderWithState(int id, String name, float price, String category, int loyaltyPoints) {
        ArrayList<Item>  standardItemArray = new ArrayList<>();
        ShoppingCart shoppingCart = new ShoppingCart(standardItemArray);
        standardItemArray.add(new StandardItem(id, name, price, category, loyaltyPoints));
        Order order = new Order(shoppingCart, customerName, customerAddress, state);
        assertEquals(standardItemArray, order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
    public void Constructor_CreateOrderWithOutState_OrderZeroState(int id, String name, float price, String category, int loyaltyPoints) {
        ArrayList<Item>  standardItemArray = new ArrayList<>();
        ShoppingCart shoppingCart = new ShoppingCart(standardItemArray);
        standardItemArray.add(new StandardItem(id, name, price, category, loyaltyPoints));
        Order order = new Order(shoppingCart, customerName, customerAddress);
        assertEquals(standardItemArray, order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }
}
