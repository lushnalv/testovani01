package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;


public class StandardItemTest {
    private final int id = 1;
    private final String name = "Dancing Panda v.2";
    private final float price = 5000F;
    private final String category = "GADGETS";
    private final int loyaltyPoints = 5;

    @Test
    public void Constructor_CreateStandardItem_StandardItem() {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        assertEquals(id, standardItem.getID());
        assertEquals(name, standardItem.getName());
        assertEquals(price, standardItem.getPrice());
        assertEquals(category, standardItem.getCategory());
        assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @Test
    public void Constructor_CopyStandardItem_StandardItem() {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem copyStandardItem = standardItem.copy();
        assertTrue(standardItem.equals(copyStandardItem));
        assertEquals(standardItem.getID(), copyStandardItem.getID());
        assertEquals(standardItem.getName(), copyStandardItem.getName());
        assertEquals(standardItem.getPrice(), copyStandardItem.getPrice());
        assertEquals(standardItem.getCategory(), copyStandardItem.getCategory());
        assertEquals(standardItem.getLoyaltyPoints(), copyStandardItem.getLoyaltyPoints());
    }

    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
//    @CsvSource({"1, name, 30.0, cat, 5", "1, name, 30.0, cat, 5"})

    public void Constructor_EqualStandardItems_WithParametrizedTest_EqualsTrue(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItem2 = new StandardItem(id, name, price, category, loyaltyPoints);

        assertTrue(standardItem.equals(standardItem2));

    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
    public void Constructor_NoEqualStandardItemsId_WithParametrizedTest_EqualsFalse(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItem2 = new StandardItem(567, name, price, category, loyaltyPoints);

        assertFalse(standardItem.equals(standardItem2));
    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
    public void Constructor_NoEqualStandardItemsName_WithParametrizedTest_EqualsFalse(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItem2 = new StandardItem(id, "ParamPam", price, category, loyaltyPoints);

        assertFalse(standardItem.equals(standardItem2));
    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
    public void Constructor_NoEqualStandardItemsPrice_WithParametrizedTest_EqualsFalse(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItem2 = new StandardItem(id, name, 1F, category, loyaltyPoints);

        assertFalse(standardItem.equals(standardItem2));
    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)
    public void Constructor_NoEqualStandardItemsCategory_WithParametrizedTest_EqualsFalse(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItem2 = new StandardItem(id, name, price, "Category", loyaltyPoints);

        assertFalse(standardItem.equals(standardItem2));
    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/dataStandardItem.csv", numLinesToSkip = 0)

    public void Constructor_NoEqualStandardItemsLoyaltyPoints_WithParametrizedTest_EqualsFalse(int id, String name, float price, String category, int loyaltyPoints) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItem2 = new StandardItem(id, name, price, category, 231098);

        assertFalse(standardItem.equals(standardItem2));
    }


}
