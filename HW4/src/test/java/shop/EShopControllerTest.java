package shop;

import archive.PurchasesArchive;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;
import storage.NoItemInStorage;
import storage.Storage;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class EShopControllerTest {
    int[] itemCount = {10,4,5};
    StandardItem[] storageItems = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
    };
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final String customerName = "Alvina";
    private final String customerAddress = "Praha, Parikova, 94/6";
    private final PrintStream standardOut = System.out;

    @BeforeEach
    public void before(){
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        for (int i = 0; i < storageItems.length; i++) {
            storage.insertItems(storageItems[i], itemCount[i]);
        }
    }
    @AfterEach
    public void after(){
        EShopController.startEShop();
        Whitebox.setInternalState(EShopController.class, "storage",(Storage)null);


    }
    @Test
    public void printListOfStoredItems_Storage_True(){
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();

        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        String outStr = "";
        for (int i = 0; i < storageItems.length; i++) {
            outStr += "STOCK OF ITEM:  Item   ID " + storageItems[i].getID()+ "   NAME " +storageItems[i].getName()+ "   CATEGORY " +storageItems[i].getCategory()
                    + "   PRICE " +storageItems[i].getPrice()+ "   LOYALTY POINTS " +storageItems[i].getLoyaltyPoints()+"    PIECES IN STORAGE: " +
                    + itemCount[i];
            if(i<storageItems.length -1){
                outStr += "\n";
            }
        }
        storage.printListOfStoredItems();
        assertEquals("STORAGE IS CURRENTLY CONTAINING:\n" + outStr + "", outputStreamCaptor.toString().trim());
        System.setOut(standardOut);
    }
    @Test
    public void insertItems_StandardItemWhichIsInStorage_True(){
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        storage.insertItems(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5), 5);
        assertTrue(15 == storage.getItemCount(1));

    }
    @Test
    public void insertItems_StandardItemWhichIsNotInStorage_True(){
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        storage.insertItems(new StandardItem(9, "Dancing Panda v.2", 5000, "GADGETS", 5), 5);
        assertTrue(5 == storage.getItemCount(9));

    }
    @Test
    public void removeItems_StandardItemWhichIsInStorage_True() throws NoItemInStorage {
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        storage.removeItems(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5), 5);
        assertTrue(5 == storage.getItemCount(1));
    }
    @Test
    public void removeItems_StandardItemWhichIsNotInStorage_Throws() throws NoItemInStorage {
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        assertThrows(NoItemInStorage.class, ()->storage.removeItems(new StandardItem(9, "Dancing Panda v.2", 5000, "GADGETS", 5), 5));
    }
    @Test
    public void processOrder_StandardItemWhichIsInStorage_True() throws NoItemInStorage {
        StandardItem standardItem1 = new StandardItem(1, "Dancing Panda v.2", 5000.0F, "GADGETS", 5);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);

        Order order = new Order(shoppingCart, customerName, customerAddress);
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        storage.processOrder(order);
        assertTrue(9 == storage.getItemCount(1));
    }
    @Test
    public void processOrder_StandardItemWhichIsInStorage_Throws() throws NoItemInStorage {
        StandardItem standardItem1 = new StandardItem(1, "Dancing Panda v.2", 5000.0F, "GADGETS", 5);
        StandardItem standardItem2 = new StandardItem(5, "Angry bird cup", 300.0F, "GADGETS", 5);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
        shoppingCart.addItem(standardItem2);
        shoppingCart.addItem(standardItem2);

        Order order = new Order(shoppingCart, customerName, customerAddress);
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        assertThrows(NoItemInStorage.class, ()->storage.processOrder(order));
    }
    @Test
    public void sortItemsByPrice_SortedItems_True(){
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        Item[] itemCollection = storage.getItemsOfCategorySortedByPrice("GADGETS").toArray(new Item[0]);

        assertEquals(storageItems[0], itemCollection[0]);
        assertEquals(storageItems[1], itemCollection[1]);
    }
    @Test
    public void getPriceOfWholeStock_GetPrice_True(){
        EShopController.startEShop();
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        assertEquals(75000F, storage.getPriceOfWholeStock());
    }

    @Test
    void purchaseShoppingCart_CertIsEmpty_PrintResult() throws NoItemInStorage {
        System.setOut(new PrintStream(outputStreamCaptor));
        EShopController.startEShop();
        ShoppingCart newCart = EShopController.newCart();
        String strOut = "Error: shopping cart is empty\n";
        EShopController.purchaseShoppingCart(newCart, customerName, customerAddress);
        assertEquals(strOut, outputStreamCaptor.toString());
        System.setOut(standardOut);
    }
    @Test
    void purchaseShoppingCart_CertIsNotEmpty_Exception() throws NoItemInStorage {
        EShopController.startEShop();
        ShoppingCart newCart = EShopController.newCart();
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[2]);
        Assertions.assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(newCart, customerName, customerAddress));

    }
    @Test
    void purchaseShoppingCart_CertIsNotEmpty_True() throws NoItemInStorage {
        EShopController.startEShop();
        ShoppingCart newCart = EShopController.newCart();
        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.addItem(storageItems[2]);
        EShopController.purchaseShoppingCart(newCart, customerName, customerAddress);
        Storage storage = Whitebox.getInternalState(EShopController.class,"storage");
        PurchasesArchive archive = Whitebox.getInternalState(EShopController.class,"archive");
        assertTrue(9 == storage.getItemCount(1));
        assertTrue(3 == storage.getItemCount(2));
        assertTrue(4 == storage.getItemCount(3));
        assertTrue(63800F == storage.getPriceOfWholeStock());
        assertTrue(1 == archive.getHowManyTimesHasBeenItemSold(storageItems[0]));
        assertTrue(1 == archive.getHowManyTimesHasBeenItemSold(storageItems[1]));
        assertTrue(1 == archive.getHowManyTimesHasBeenItemSold(storageItems[2]));
    }


}