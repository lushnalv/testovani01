package storage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    int id = 1;
    String name = "Dancing Panda v.2";
    float price = 5000F;
    String category = "GADGETS";
    int loyaltyPoints = 5;
    StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
    int count = 0;


    @Test
    public void Constructor_ItemStock_ItemStock(){
        String string = "STOCK OF ITEM:  "+standardItem.toString()+"    PIECES IN STORAGE: "+count;
        ItemStock itemStock = new ItemStock(standardItem);
        assertEquals(count, itemStock.getCount());
        assertEquals(standardItem, itemStock.getItem());
        assertTrue(string.equals(itemStock.toString()));

    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/countItemIncrease.csv", numLinesToSkip = 0)
    public void Increase_ItemStockCountIncrease_CountIncrease(int count, int result){
        ItemStock itemStock = new ItemStock(standardItem);
        itemStock.increaseItemCount(count);
        assertEquals(result, itemStock.getCount());
    }
    @ParameterizedTest()
    @CsvFileSource(resources = "/countItemDecrease.csv", numLinesToSkip = 0)
    public void Decrease_ItemStockCountDecrease_CountDecrease(int count, int result){
        ItemStock itemStock = new ItemStock(standardItem);
        itemStock.increaseItemCount(10000);
        itemStock.decreaseItemCount(count);
        assertEquals(result, itemStock.getCount());
    }

}