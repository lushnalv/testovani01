package archive;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PurchasesArchiveTest {

    private int id = 1;
    private String name = "Dancing Panda v.2";
    private float price = 5000F;
    private String category = "GADGETS";
    private int loyaltyPoints = 5;
    private ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private String customerName = "Alvina";
    private String customerAddress = "Praha, Parikova, 94/6";
    private final PrintStream standardOut = System.out;

    @BeforeEach
    public void before() {
        id = 1;
        name = "Dancing Panda v.2";
        price = 5000F;
        category = "GADGETS";
        loyaltyPoints = 5;
        customerName = "Alvina";
        customerAddress = "Praha, Parikova, 94/6";
    }


    @Test
    public void getHowManyTimesHasBeenItemSold_ItemIsNotInArchive_True() {
        PurchasesArchive purchasesArchive = mock(PurchasesArchive.class);
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        assertTrue(0 == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem));
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_ItemIsInArchive_True() {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(standardItem);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(standardItem.getID(), itemPurchaseArchiveEntry);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, new ArrayList<Order>());

        assertTrue(1 == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem));
    }

    @ParameterizedTest
    @CsvSource({"1, 2", "5, 6", "1000, 1001", "999, 1000", "300, 301"})
    public void getHowManyTimesHasBeenItemSold_ItemsIsInArchive_True(int increaseCount, int count) {
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(standardItem);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(standardItem.getID(), itemPurchaseArchiveEntry);
        itemArchive.get(standardItem.getID()).increaseCountHowManyTimesHasBeenSold(increaseCount);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, new ArrayList<Order>());

        assertTrue(count == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem));
    }

    @Test
    public void printItemPurchaseStatistics_StandardPrint_Equals() {
        System.setOut(new PrintStream(outputStreamCaptor));
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(standardItem);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(standardItem.getID(), itemPurchaseArchiveEntry);
        itemArchive.get(standardItem.getID()).increaseCountHowManyTimesHasBeenSold(5);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, new ArrayList<Order>());
        purchasesArchive.printItemPurchaseStatistics();
        assertEquals("ITEM PURCHASE STATISTICS:\n" + itemArchive.get(standardItem.getID()) + "", outputStreamCaptor.toString().trim());
        System.setOut(standardOut);
    }

    @Test
    public void putOrderToPurchasesArchive_StandardItem_True() {

        StandardItem standardItem1 = new StandardItem(1, "Dancing Panda v.2", 5000.0F, "GADGETS", 5);
        StandardItem standardItem2 = new StandardItem(5, "Angry bird cup", 300.0F, "GADGETS", 5);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
        shoppingCart.addItem(standardItem2);
        shoppingCart.addItem(standardItem2);

        Order order = new Order(shoppingCart, customerName, customerAddress);

        PurchasesArchive purchasesArchive = new PurchasesArchive();

        purchasesArchive.putOrderToPurchasesArchive(order);
        assertTrue(1 == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem1));
        assertTrue(2 == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem2));
    }

    @Test
    public void putOrderToPurchasesArchive_MockOrderArchive_True() {
        StandardItem standardItem1 = new StandardItem(1, "Dancing Panda v.2", 5000.0F, "GADGETS", 5);
        StandardItem standardItem2 = new StandardItem(5, "Angry bird cup", 300.0F, "GADGETS", 5);

        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry1 = new ItemPurchaseArchiveEntry(standardItem1);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry2 = new ItemPurchaseArchiveEntry(standardItem2);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();

        itemArchive.put(standardItem1.getID(), itemPurchaseArchiveEntry1);
        itemArchive.put(standardItem2.getID(), itemPurchaseArchiveEntry2);
        itemArchive.get(standardItem1.getID()).increaseCountHowManyTimesHasBeenSold(5);
        itemArchive.get(standardItem2.getID()).increaseCountHowManyTimesHasBeenSold(5);

        ArrayList<Order> orderArchive = mock(ArrayList.class);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchive);
        assertTrue(6 == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem1));
        assertTrue(6 == purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem2));

    }

    @Test
    public void putOrderToPurchasesArchive_MockItemPurchaseArchiveEntry_True() {
        System.setOut(new PrintStream(outputStreamCaptor));
        StandardItem standardItem = new StandardItem(1, "Dancing Panda v.2", 5000.0F, "GADGETS", 5);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();

        ItemPurchaseArchiveEntry mock = mock(ItemPurchaseArchiveEntry.class);
        when(mock.toString()).thenReturn("mock ItemPurchaseArchiveEntry");

        itemArchive.put(0, mock);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem);

        Order order = new Order(shoppingCart, customerName, customerAddress);
        ArrayList<Order> orderArchive = new ArrayList<>();
        orderArchive.add(order);

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchive);
        purchasesArchive.putOrderToPurchasesArchive(order);
        purchasesArchive.printItemPurchaseStatistics();

        assertEquals("Item with ID 1 added to the shopping cart.\nITEM PURCHASE STATISTICS:\n" + "mock ItemPurchaseArchiveEntry\n" + itemArchive.get(standardItem.getID()) + "", outputStreamCaptor.toString().trim());
        System.setOut(standardOut);
    }


}